package com.app.proj.webservice;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.app.proj.backend.entity.User;
import com.app.proj.backend.service.UserService;
import com.app.proj.webapp.util.JwtTokenUtil;

/**
 * @author HsuWaiWaiTun
 *
 */

@RestController
public class AdminLoginController {

	
	private static final Logger log = LoggerFactory.getLogger(AdminLoginController.class);

	@Autowired
	UserService userService;
	
	@Autowired
	JwtTokenUtil jwtTokenUtil;
	
	@RequestMapping(value = "/api/admin/login", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<?> createAuthenticationToken(HttpServletResponse response,HttpServletRequest request,@RequestBody String data) {
		log.debug("Logging In" + data);
		try {
			JSONObject jObject = new JSONObject(data);
			String username = jObject.getString("username");
			String password = jObject.getString("password");
			User user = userService.login(username, password);
			
			if(user != null){
				//String token = jwtTokenUtil.generateToken(user);
				return new ResponseEntity<>(user,HttpStatus.OK);
			}else{
				return new ResponseEntity<>("Authentication Error",HttpStatus.UNAUTHORIZED);
			}
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}

}
