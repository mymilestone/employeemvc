package com.app.proj.backend.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.app.proj.backend.entity.Employee;
import com.app.proj.backend.entity.Payment;
import com.app.proj.backend.entity.Salary;
import com.app.proj.backend.service.EmployeeService;
import com.app.proj.backend.service.PaymentService;
import com.app.proj.backend.service.SalaryService;

/**
 * @author HsuWaiWaiTun
 *
 */
@Controller
public class SalaryController {
	
	private static final Logger log = LoggerFactory.getLogger(SalaryController.class);
	
	@Autowired
	EmployeeService employeeService;
	
	@Autowired
	PaymentService paymentService;
	
	@Autowired
	SalaryService salarySerivce;
	
	@RequestMapping(value = "/salary", method = RequestMethod.POST)
	public String initiatePayment(@ModelAttribute("paymentId") int paymentId){
		log.debug("Payment "+ paymentId);
		
		if(paymentId == 2){
			return "redirect:/salary/full_time";
		}else{
			return "redirect:/salary/part_time";	
		}
		
	}

	@RequestMapping(value= "/salary/full_time",  method= RequestMethod.GET)
	public ModelAndView salaryFullTime(){
		ModelAndView modelView = new ModelAndView("salary_full_time");
		List<Employee> empList = employeeService.findEmployeesByPaymentType(2);
		Payment payment = paymentService.findByPaymentId(2);
		modelView.addObject("payment", payment);
		modelView.addObject("empList", empList);
		return modelView;
	}
	
	@RequestMapping(value= "/salary/part_time",  method= RequestMethod.GET)
	public ModelAndView salaryPartTime(){
		ModelAndView modelView = new ModelAndView("salary_part_time");
		List<Employee> empList = employeeService.findEmployeesByPaymentType(1);
		Payment payment = paymentService.findByPaymentId(1);
		modelView.addObject("payment", payment);
		modelView.addObject("empList", empList);
		return modelView;
	}
	
	@RequestMapping(value = "/salary/full_time/calculate", method = RequestMethod.POST)
	public ModelAndView calculateFullTime(@ModelAttribute("empId") int empId, @ModelAttribute("attendance") int attendance, @ModelAttribute("overtime") Double overtime,
				@ModelAttribute("allowance") Double allowance
			){
		Salary salary = new Salary();
		
		Employee employee = employeeService.findById(empId);

		salary.setAllowance(allowance);
		salary.setSalaryBasic(paymentService.findByPaymentId(2).getPaymentRate());
		salary.setSalaryOvertime(overtime);
		salary.setWorkingDays(attendance);
		
		Double totalSalary = salary.getSalaryBasic() * salary.getWorkingDays() + salary.getAllowance() + salary.getSalaryOvertime();
		salary.setTotalSalary(totalSalary);
		log.debug(salary.toString());
		salarySerivce.addSalary(salary);
		employee.setSalary(salary);
		employeeService.updateEmployee(employee);

		
		ModelAndView modelView = new ModelAndView();
		List<Payment> paymentList = paymentService.findAllPayment();
		modelView.setViewName("index");
		modelView.addObject("paymentList", paymentList);
		modelView.addObject("message", "Calculation for "+employee.getName()+" is Successful.");
		return modelView;
	}
	
	@RequestMapping(value = "/salary/part_time/calculate", method = RequestMethod.POST)
	public ModelAndView calculatePartTime(@ModelAttribute("empId") int empId, @ModelAttribute("hours") int hours
			){
		Salary salary = new Salary();
		
		Employee employee = employeeService.findById(empId);

		salary.setSalaryBasic(paymentService.findByPaymentId(1).getPaymentRate());
		salary.setWorkingHours(hours);
		
		Double totalSalary = salary.getSalaryBasic() * salary.getWorkingHours();
		salary.setTotalSalary(totalSalary);
		log.debug(salary.toString());
		salarySerivce.addSalary(salary);
		employee.setSalary(salary);
		employeeService.updateEmployee(employee);
		ModelAndView modelView = new ModelAndView();
		List<Payment> paymentList = paymentService.findAllPayment();
		modelView.setViewName("index");
		modelView.addObject("paymentList", paymentList);
		modelView.addObject("message", "Calculation for "+employee.getName()+" is Successful.");
		return modelView;
	}
}
