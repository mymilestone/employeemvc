package com.app.proj.webapp.util;

import org.springframework.stereotype.Component;

/**
 * @author HsuWaiWaiTun
 *
 */
@Component
public class ReportUtil {

	private String name;
	private String email;
	private String paymentType;
	private Double paymentRate;
	private String workingDays;
	private String workingHours;
	private Double totalSalary;
	
	public ReportUtil() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ReportUtil(String name, String email, String paymentType, Double totalSalary) {
		super();
		this.name = name;
		this.email = email;
		this.paymentType = paymentType;
		this.totalSalary = totalSalary;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public Double getTotalSalary() {
		return totalSalary;
	}

	public void setTotalSalary(Double totalSalary) {
		this.totalSalary = totalSalary;
	}


	public Double getPaymentRate() {
		return paymentRate;
	}

	public void setPaymentRate(Double paymentRate) {
		this.paymentRate = paymentRate;
	}

	public String getWorkingDays() {
		return workingDays;
	}

	public void setWorkingDays(String workingDays) {
		this.workingDays = workingDays;
	}

	public String getWorkingHours() {
		return workingHours;
	}

	public void setWorkingHours(String workingHours) {
		this.workingHours = workingHours;
	}

	@Override
	public String toString() {
		return "ReportUtil [name=" + name + ", email=" + email + ", paymentType=" + paymentType + ", totalSalary="
				+ totalSalary + "]";
	}
	
	
}
