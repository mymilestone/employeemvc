package com.app.proj.backend.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.app.proj.backend.entity.Employee;
import com.app.proj.backend.entity.Payment;
import com.app.proj.backend.entity.Salary;
import com.app.proj.backend.service.EmployeeService;
import com.app.proj.backend.service.PaymentService;
import com.app.proj.backend.service.SalaryService;
import com.app.proj.webapp.util.ReportUtil;

/**
 * @author HsuWaiWaiTun
 *
 */
@Controller
public class ReportController {
	
	private static final Logger log = LoggerFactory.getLogger(ReportController.class);
	
	@Autowired
	EmployeeService employeeService;
	
	@Autowired
	PaymentService paymentService;
	
	@Autowired
	SalaryService salaryService;
	
	@Autowired
	ReportUtil reportUtil;

	@RequestMapping(value="/report", method = RequestMethod.GET)
	public ModelAndView generate(){
		log.debug("in generating report");
		ModelAndView modelView = new ModelAndView();
		List<ReportUtil> reportList= new ArrayList<ReportUtil>();
		ReportUtil report;
		List<Salary> salaryList = salaryService.findAllSalaries();
		for(Salary salary: salaryList){
			//Employee emp = salary.getEmployee();
			Employee emp = employeeService.findEmployeeBySalary(salary.getSalaryId());
			log.debug(emp.toString());
			Payment payment = emp.getPayment();
			log.debug(payment.toString());
			//Salary emp_salary = salaryService.findSalaryByEmpId(emp.getId());
			Salary emp_salary = emp.getSalary();
			log.debug(emp_salary.toString());
			report = new ReportUtil();
			report.setName(emp.getName());
			report.setEmail(emp.getEmail());
			report.setPaymentType(payment.getPaymentType());
			report.setPaymentRate(payment.getPaymentRate());
			report.setWorkingDays(emp_salary.getWorkingDays() == 0 ? "-" : String.valueOf(emp_salary.getWorkingDays()));
			report.setWorkingHours(emp_salary.getWorkingHours() == 0 ? "-" : String.valueOf(emp_salary.getWorkingHours()));
			report.setTotalSalary(emp_salary.getTotalSalary());
			
			log.debug(report.toString());
			reportList.add(report);
		}
		modelView.setViewName("report");
		modelView.addObject("reportList", reportList);
		return modelView;
	}
}
