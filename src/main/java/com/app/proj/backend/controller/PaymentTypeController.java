package com.app.proj.backend.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import com.app.proj.backend.entity.Payment;
import com.app.proj.backend.service.PaymentService;

/**
 * @author HsuWaiWaiTun
 *
 */
@Controller
public class PaymentTypeController {

	
	private static final Logger log = LoggerFactory.getLogger(PaymentTypeController.class);
	
	@Autowired
	PaymentService paymentService;
	
	@RequestMapping(value="/payment/all", method=RequestMethod.GET)
	public ModelAndView paymentTypeList(Model model){
		log.debug("All Payment Types ............");
		List<Payment> paymentList = paymentService.findAllPayment();

		ModelAndView modelView = new ModelAndView("payment_list");
		modelView.addObject("paymentList", paymentList);
		return modelView;
	}
	
	@RequestMapping(value="/payment/add", method=RequestMethod.GET)
	public ModelAndView addPayment(Model model){
		log.debug("For New Payment");
		Payment payment = new Payment();
		ModelAndView modelView = new ModelAndView("payment_create");
		modelView.addObject("payment", payment);
		return modelView;
	}
	
	@RequestMapping(value = "/payment/save", method = RequestMethod.POST)
	public String savePayment(HttpServletRequest request, @ModelAttribute Payment payment, BindingResult bindResult) {
		log.debug("Save Payment");
		boolean flag=false; 
		flag = ( payment.getPaymentId() == null ? true: false);

	    if(flag){
	    	paymentService.addPayment(payment);
	    }else{
	    	paymentService.updatePayment(payment);
	    }

		return "redirect:/payment/all";
	}
	
	@RequestMapping(value="/payment/edit", method=RequestMethod.POST)
	public ModelAndView editPayment(Model model, @ModelAttribute("id") Integer id){
		log.debug("Edit Payment " + id);
		Payment payment = paymentService.findByPaymentId(id);
		ModelAndView modelView = new ModelAndView("payment_edit");
		modelView.addObject("payment", payment);
		return modelView;
	}
	
	@RequestMapping(value="/payment/delete", method=RequestMethod.POST)
	public String deletePayment(Model model, @ModelAttribute("id") Integer id){
		log.debug("Delete Payment");
		paymentService.deletePayment(id);
		log.debug("Delete Successful " + id);
		return "redirect:/payment/all";
	}
	
}
